package com.vivo.service;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Service;

@Service
public class MessageConsumer implements MessageListener {
    @Override
    public void onMessage(Message message) {
        System.out.print("接收到消息："+new String(message.getBody()));
    }
}