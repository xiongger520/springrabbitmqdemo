package com.vivo.controller;

import com.vivo.service.MessageConsumer;
import com.vivo.service.MessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringRabbitMQController {
    @Autowired
    private MessageProducer messageProducer;

    @RequestMapping("/sendMessage")
    public void sendMessage(String tmp) {
        messageProducer.sendMessage(tmp);
    }
}
